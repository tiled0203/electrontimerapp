module.exports = {
  packagerConfig: {},
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {
        authors: 'Timothy Leduc',
        description: 'An example Electron timer app'
      }
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin'],
    },
    {
      name: '@electron-forge/maker-deb',
      config: {
        authors: 'Timothy Leduc',
        description: 'An example Electron timer app'
      }
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {
        authors: 'Timothy Leduc',
        description: 'An example Electron timer app'
      }
    },
  ],
};
