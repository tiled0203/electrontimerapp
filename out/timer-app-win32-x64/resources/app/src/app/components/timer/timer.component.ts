import {Component} from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent {
  currentValue: number = 0;
  timerSetValue: number = 0;
  timerTitle: number = 0;
  private interval: NodeJS.Timer | undefined;
  private play: boolean = false;

  start() {
    this.play = true;
    this.currentValue = ((this.timerSetValue / 60) * 100) / (this.timerSetValue / 60);
    this.timerTitle = this.timerSetValue;
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      if (this.currentValue > 0) {
        this.currentValue -= (100 / this.timerSetValue);
        this.timerTitle -= 1;
      } else {
        this.stop();
      }
    }, 1000);
  }

  stop() {
    this.play = false;
    clearInterval(this.interval);
  }

  calculateTime(): string {
    return ((this.currentValue * 60) / 100) + '';
  }
}
