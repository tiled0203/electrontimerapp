import {Component} from '@angular/core';
import {WebsocketService} from "../../services/websocket.service";
import {Message} from "../../models/Message";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent {
  content = '';
  received: Message[] = [];
  sent: Message[] = [];
  websocketsUrl!: string;

  constructor(private websocketService: WebsocketService) {

  }

  sendMsg() {
    let message: Message = {
      source: '',
      content: ''
    };
    message.source = 'electron';
    message.content = this.content;

    this.sent.push(message);
    this.websocketService.messages.next(message);
  }

  connect() {
    this.websocketService.connect(this.websocketsUrl).subscribe(_value => {
      this.websocketService.messages.subscribe(msg => {
        this.received.unshift(msg);
        console.log("Response from websocket: " + msg);
      });
    });
  }
}
