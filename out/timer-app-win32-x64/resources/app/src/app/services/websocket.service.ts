import {Injectable} from '@angular/core';
import {AnonymousSubject, Subject} from 'rxjs/internal/Subject';
import {map, Observable, Observer} from "rxjs";
import {Message} from "../models/Message";

const CHAT_URL = "ws://localhost:8080/jee7-websockets-solution/chat";

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private subject!: AnonymousSubject<MessageEvent>;
  public messages!: Subject<Message>;

  constructor() {

  }

  public connect(url: string): AnonymousSubject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
      console.log("Successfully connected: " + url);
      this.messages = <Subject<Message>>this.subject.pipe(
        map(
          (response: MessageEvent): Message => {
            console.log(JSON.parse(response.data));
            return JSON.parse(response.data)
          }
        )
      );
    }
    return this.subject;
  }

  private create(url: string | URL): AnonymousSubject<MessageEvent> {
    let ws = new WebSocket(url);
    let observable = new Observable((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer: Observer<MessageEvent> = {
      complete(): void {
      },
      error(_err: any): void {
      },
      next: (data: Object) => {
        console.log('Message sent to websocket: ', data);
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };
    return new AnonymousSubject<MessageEvent>(observer, observable);
  }

}
