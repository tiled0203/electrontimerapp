import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "../../components/home/home.component";
import {TimerComponent} from "../../components/timer/timer.component";
import {ChatComponent} from "../../components/chat/chat.component";

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'timer', component: TimerComponent},
  {path: 'chat', component: ChatComponent},
  {path: '**', redirectTo: "home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule {
}
